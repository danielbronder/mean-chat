const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exports = module.exports;

const ChatmessageSchema = new mongoose.Schema({
  author: {
    type: String,
  },
  message: {
    type: String,
  },
  chatroom: {
    type: String,
  }
});

const Chatmessage = mongoose.model('Chatmessage', ChatmessageSchema);
module.exports = Chatmessage;
