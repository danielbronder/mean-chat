const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var exports = module.exports;

const ChatroomSchema = new mongoose.Schema({
  author: {
    type: String,
    required: true
  },
  name: {
    type: String,
    unique: true,
    required: true
  }
});

const Chatroom = mongoose.model('Chatroom', ChatroomSchema);
module.exports = Chatroom;
