const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);

//connectChatroom to MongoDB
mongoose.connect('mongodb://localhost/Chat');
const db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log("Connected to MongoDB");
});

//use sessions for tracking logins
app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// include routes
const user = require('./server/routes/user');
const chat = require('./server/routes/chatmessages');
const chatroom = require('./server/routes/chatroom');
app.use('/', user);
app.use('/', chat);
app.use('/', chatroom);

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist','/index.html'))
});

io.on('connection', (socket) => {
  console.log('A user is connected');

  socket.on('disconnect', function () {
    console.log('A user is disconnected')
  });

  socket.on('chatroom', (message) => {
    io.emit('chatroom', { type: 'message', name: message});
  });

  socket.on('chatmessages', (message) => {
    io.emit('chatmessages', { type: 'message', chatroom: message.chatroom, author: message.author, message: message.message});
  });
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function (err, req, res) {
  res.status(err.status || 500);
  res.send(err.message);
});

const port = process.env.PORT || '3000';
app.set('port', port);

server.listen(port, () => console.log("Running on port: " + port));
