import { Injectable }       from '@angular/core';
import { WebsocketService } from './websocket.service';
import { Subject }          from 'rxjs/Subject';

@Injectable()
export class SocketService {

  messages: Subject<any>;

  constructor(private webService: WebsocketService) {
    this.messages = <Subject<any>>this.webService
      .connectChatroom()
      .map((response: any): any => {
        return response;
      });
  }
}
