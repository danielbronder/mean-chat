import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as Rx from 'rxjs/RX';
import * as io from 'socket.io-client';
import {environment} from "../../environments/environment";

@Injectable()
export class WebsocketService {

  private socket;

  constructor () {}

  connectChatroom(): Rx.Subject<MessageEvent> {

    this.socket = io(environment);

    const observable = new Observable(observer => {
      this.socket.on('chatroom', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });

    const observer = {
      next: (data: Object) => {
        this.socket.emit('chatroom', data);
      }
    };

    return Rx.Subject.create(observer, observable);
  }

  connectChatmessages(): Rx.Subject<MessageEvent> {

    this.socket = io(environment);

    const observable = new Observable(observer => {
      this.socket.on('chatmessages', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });

    const observer = {
      next: (data: Object) => {
        this.socket.emit('chatmessages', data);
      }
    };

    return Rx.Subject.create(observer, observable);
  }
}
