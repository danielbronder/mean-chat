import { Component, OnInit } from "@angular/core";

import { ChatroomService } from "./chatroom.service";
import { Chatroom } from "./chatroom";
import { Chat } from "../chat/chat";
import { ChatService } from "../chat/chat.service";
import { UserService } from "../userinfo/user.service";

@Component ({
  selector: 'app-chatrooms',
  templateUrl: './chatroom.component.html',
  styleUrls: ['../app.component.css'],
  providers: [ ChatroomService ]
})

export class ChatroomComponent implements OnInit {

  private title = "";
  public room = "";

  private chatroom = new Chatroom('');
  private chat = new Chat('', '', '');

  public  chatrooms = [];
  public  chatmessages = [];

  public  user;

  constructor(private chatroomService: ChatroomService,
              private chatService: ChatService,
              private userService: UserService) { }

  createChatroom(term: string): void {
    this.chatroom = new Chatroom(term);

    this.chatroomService.createChatroom(this.chatroom)
      .subscribe(
        chatroomMsg => {
          this.chatroom = chatroomMsg;
        },
        error =>  this.title = <any>error
      );
  }

  getChatrooms() {

    this.chatroomService.getChatrooms()
      .subscribe(chatrooms => {
          this.chatrooms = chatrooms;
        }, error => this.title = <any>error
      );
  }

  getChatroom(term: string) {
    this.room = term;
    this.getMessages(this.room);
  }

  sendMessage(message: string) {

    if (this.room) {
      this.chat = new Chat(this.user, message, this.room);
    }

    this.chatService.sendMessage(this.chat)
      .subscribe(
        chatroomMsg => {
          this.chat = chatroomMsg;
        },
        error =>  this.title = <any>error
      );
  }

  getMessages(term: string) {
    this.room = term;
    this.chatroom = new Chatroom(term);

    this.chatService.getMessages(this.chatroom)
      .subscribe(message => {
        this.chatmessages = message;
        }, error => this.title = <any>error
      );
  }

  getUser() {
    this.userService.getUser()
      .subscribe(
        user => {
          this.user = user.username;
        },
        error => this.user = <any>error
      );
  }

  ngOnInit(): void {
    this.getUser();
    this.getChatrooms();
    this.chatroomService.messages.subscribe(chatroom => {
      if (chatroom) {
        this.chatrooms.push(chatroom);
      }
    });
    this.chatService.messages.subscribe(chatmessage => {
      if (chatmessage.chatroom === this.room) {
        this.chatmessages.push(chatmessage);
      }
    });
  }

}
