import { Injectable }       from "@angular/core";
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable }     from "rxjs/Observable";

import '../utils/rxjs-operators';

import { WebsocketService } from "../socket/websocket.service";
import { Subject } from "rxjs/Subject";
import { Chatroom } from "./chatroom";

@Injectable()
export class ChatroomService {

  private chatroomUrl = "/chatroom";
  // private chatUrl = "/chat";

  messages: Subject<any>;

  constructor(private webService: WebsocketService, private http: Http) {
    this.messages = <Subject<any>>this.webService
      .connectChatroom()
      .map((response: any): any => {
        return response;
      });
  }

  createChatroom(chatroom: Chatroom): Observable<Chatroom> {

  const headers = new Headers({ 'Content-Type': 'application/json' });
  const options = new RequestOptions({ headers: headers });

  this.messages.next(chatroom.chatroom);

  return this.http.post(this.chatroomUrl, chatroom, options)
    .map(this.extractData)
    .catch(this.handleError);

  }

  /**
   * @returns Chatrooms from server
   */
  getChatrooms (): Observable<Chatroom[]> {
    return this.http.get(this.chatroomUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }


  private extractData(res: Response) {
    const body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
