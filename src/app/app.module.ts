import { BrowserModule }      from '@angular/platform-browser';
import { NgModule }           from '@angular/core';
import { RouterModule }       from '@angular/router';
import { HttpModule}          from '@angular/http';

import { NgbModule }          from '@ng-bootstrap/ng-bootstrap';

import { AppComponent }       from './app.component';
import { LoginComponent }     from './login/login.component';
import { RegisterComponent }  from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule }   from './app.routing-module';

import { ChatroomComponent }  from './chatrooms/chatroom.component';

import { SocketService }      from './socket/socket.service';
import { WebsocketService }   from './socket/websocket.service';
import { ChatroomService }    from './chatrooms/chatroom.service';
import { ChatService }        from './chat/chat.service';
import { UserService }        from './userinfo/user.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    ChatroomComponent,
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpModule,

  ],
  providers: [
    RegisterComponent,
    ChatService,
    SocketService,
    WebsocketService,
    ChatroomService,
    UserService,
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
