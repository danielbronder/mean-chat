export class Chat {
  constructor(
    public author: string,
    public message: string,
    public chatroom: string
  ) {  }
}
