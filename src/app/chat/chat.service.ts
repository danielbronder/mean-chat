import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Subject } from "rxjs/Subject";
import { WebsocketService } from "../socket/websocket.service";
import { Chat } from "./chat";
import '../utils/rxjs-operators';
import {Chatroom} from "../chatrooms/chatroom";

@Injectable()
export class ChatService {

  private chatUrl = "/chat";
  messages: Subject<any>;

  constructor(private webService: WebsocketService, private http: Http) {
    this.messages = <Subject<any>>this.webService
      .connectChatmessages()
      .map((response: any): any => {
        return response;
      });
  }

  sendMessage(chat: Chat): Observable<Chat> {

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    this.messages.next(chat);

    return this.http.post(this.chatUrl, chat, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getMessages(chatroom: Chatroom): Observable<Chat[]> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers, params: chatroom });

    return this.http.get(this.chatUrl, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*
   * Data handlers
   */
  private extractData(res: Response) {
    const body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
