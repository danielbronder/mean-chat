import { Component, OnInit } from '@angular/core';
import { SocketService }       from '../socket/socket.service';

@Component({
  selector: 'app-login',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../app.component.css']
})

export class DashboardComponent implements OnInit {
  messages = [];

  constructor(private socket: SocketService) { }

  ngOnInit(): void {
    this.socket.messages.subscribe(message => {
      if (message) {
        this.messages.push(message);
      }
    });
  }

}
