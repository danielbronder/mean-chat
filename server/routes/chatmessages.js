const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const Chat = require('../../models/chatmessages');

router.post('/chat', function (req, res, next) {

  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        console.log(error);
        return next(error);
      } else {
        if (user === null) {
          const err = new Error("Not authorized! please login");
          err.status = 400;
          return res.send('<h1>You are not authorized to create a chatroom!</h1><a type="button" href="/login">Click here to login</a>');
        } else {

          if (req.body.message && req.body.chatroom && req.body.author) {

            const message = {
              author: req.body.author,
              message: req.body.message,
              chatroom: req.body.chatroom
            };

            Chat.create(message, function (error) {
              if(error){
                console.log(error);
                return next(error);
              }
            });
          }

        }
      }
    });

});

router.get('/chat', function (req, res, next) {

  Chat.find(req.query).exec(function (error, schema) {
    if (error) {
      return next(error);
    } else {
      res.send(schema);
    }
  });
});

//export the router
module.exports = router;
