const express = require('express');
const router = express.Router();
const User = require('../../models/user');

//POST route for creating a user
router.post('/register', function (req, res, next) {

  // confirm that user typed same password twice
  if (req.body.password !== req.body.passwordconf) {
    const err = new Error("Passwords do not match!");
    err.status = 400;
    res.send("Passwords do not match!");
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordconf) {

    const userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordconf: req.body.passwordconf,
    };

    User.create(userData, function (error, user) {
      if (error) {
        console.log(error);
        return next(error);
      } else {
        req.session.userId = user._id;
        return res.redirect('/dashboard');
      }
    });

  } else {

    const err = new Error("Credentials are not correct");
    err.status = 400;
    res.send("Login credentials are not correct");
    return next(err);

  }});

router.post('/login', function (req, res) {

  if (req.body.email && req.body.password) {
    User.authenticate(req.body.email, req.body.password, function (error, user) {
      if (error || !user) {
        return res.send('<h1>Wrong e-mail or password! Pleas try again!</h1><a type="button" href="/login">Click here to login again</a>');
      } else {
        req.session.userId = user._id;
        return res.redirect('/dashboard');
      }
    });
  }
});

// GET route after registering
router.get('/profile', function (req, res, next) {

  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          const err = new Error("Not authorized! Go back!");
          err.status = 400;
          return next(err);
        } else {
          res.send(user.username);
        }
      }
    });
});

// GET for  logout
router.get('/logout', function (req, res, next) {

  if (req.session) {
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

module.exports = router;
