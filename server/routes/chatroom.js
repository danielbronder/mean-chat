const express = require('express');
const router = express.Router();
const Chatroom = require('../../models/chatroom');
const User = require('../../models/user');

router.clients = [];

/**
 * Create new chatroom
 */
router.post('/chatroom', function (req, res, next) {

  User.findById(req.session.userId)
    .exec(function (error, user) {
      if (error) {
        console.log(error);
        return next(error);
      } else {
        if (user === null) {
          const err = new Error("Not authorized! please login");
          err.status = 400;
          return res.send('<h1>You are not authorized to create a chatroom!</h1><a type="button" href="/login">Click here to login</a>');
        } else {

          if (req.body.chatroom && user.username) {

            const chatroom = {
              author: user.username,
              name: req.body.chatroom
            };

            Chatroom.create(chatroom, function (error) {
              if(error){
                return next(error);
              }
            });
          }

        }
      }
    });

});

/**
 * Get all chatrooms
 */
router.get('/chatroom', function (req, res, next) {

  Chatroom.find({}).exec(function (error, schema) {
    if (error) {
      return next(error);
    } else {
      res.send(schema);
    }
  });
});

module.exports = router;
