# Chat

This is a chat application according to the MEAN principles. 

This project contains the following dependencies:

Client:

* Bootstrap
* Angular 5

Server:

* ExpressJS
* Socket.io

Database:

* Mongoose
* MongoDB

Test :
 
 * Jasmine
 * Karma
 * Mocha
 * Chai

## Setup

1. Install all dependencies with 'npm install'
2. Build the project with 'ng build' or 'npm run-scripts build'
3. Run the application with 'node app.js' or 'npm run-scripts start'

## Test

1. To run the Angular test execute 'npm test'
2. To run the ExpressJS test execute 'mocha'
