const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const chai = require('chai');
const expect = chai.expect;

const Chatroomschema = new Schema({
  author: { type: String, required: true },
  name: { type: String, required: true }
});

//Create a new collection called 'Chatroom'
const Chatroom = mongoose.model('Chatroom', Chatroomschema);

describe('Database Test', function() {

  //Connection to the database
  before(function (done) {
    mongoose.connect('mongodb://localhost/Chat');
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function() {
      console.log('We are connected to Chat database!');
      done();
    });
  });
  describe('Chat Database', function() {

    //Save object with 'chatroom' value of "Daniel" and "Chatrooom 1"
    it('New chatroom saved to chat database', function(done) {
      const chatroom = Chatroom({
        author: "Daniel",
        name: "Chatroom 1"
      });

      chatroom.save(done);
    });

    it('Save incorrect chatroom in the database', function(done) {

      //Attempt to save with wrong info.
      const incorrectChatroom = Chatroom({
        user: "Daniel",
        name: "Chatroom 1"
      });
      incorrectChatroom.save(err => {
        if(err) { return done(); }
        throw new Error('Chatroom is not correct');
      });
    });

  });

  //After all tests are finished drop database and close connection
  after(function(done){
    mongoose.connection.db.dropDatabase(function(){
      mongoose.connection.close(done);
    });
  });
});
